#ifndef __APPTHEME_H
#define __APPTHEME_H

#include <QObject>
#include <QColor>

class CAppTheme: public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY(int horizontalPageMargin READ horizontalPageMargin NOTIFY horizontalPageMarginChanged);

    Q_PROPERTY(int paddingLarge READ paddingLarge NOTIFY paddingLargeChanged);
    Q_PROPERTY(int paddingMedium READ paddingMedium NOTIFY paddingMediumChanged);
    Q_PROPERTY(int paddingSmall READ paddingSmall NOTIFY paddingSmallChanged);

    Q_PROPERTY(QColor primaryColor READ primaryColor NOTIFY primaryColorChanged);
    Q_PROPERTY(QColor highlightColor READ highlightColor NOTIFY highlightColorChanged);

    Q_PROPERTY(QColor secondaryColor READ secondaryColor NOTIFY secondaryColorChanged);
    Q_PROPERTY(QColor secondaryHighlightColor READ secondaryHighlightColor NOTIFY secondaryHighlightColorChanged);

    Q_PROPERTY(QColor highlightBackgroundColor READ highlightBackgroundColor NOTIFY highlightBackgroundColorChanged);

    Q_PROPERTY(int fontSizeLarge READ fontSizeLarge NOTIFY fontSizeLargeChanged)

signals:
    void horizontalPageMarginChanged();
    void paddingLargeChanged();
    void paddingMediumChanged();
    void paddingSmallChanged();
    void primaryColorChanged();
    void highlightColorChanged();
    void secondaryColorChanged();
    void secondaryHighlightColorChanged();
    void highlightBackgroundColorChanged();
    void fontSizeLargeChanged();

public:
    int horizontalPageMargin()
    {
        return 16;
    }

    int paddingLarge()
    {
        return 8;
    }

    int paddingMedium()
    {
        return 6;
    }

    int paddingSmall()
    {
        return 4;
    }

    QColor primaryColor()
    {
        return QColor("black");
    }

    QColor highlightColor()
    {
        return QColor("gray");
    }

    QColor secondaryColor()
    {
        return QColor("gray");
    }

    QColor secondaryHighlightColor()
    {
        return QColor("darkgray");
    }

    QColor highlightBackgroundColor()
    {
        return QColor("lightgray");
    }

    int fontSizeLarge()
    {
        return 28;
    }
};

#endif // __APPTHEME_H
