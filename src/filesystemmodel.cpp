#include "filesystemmodel.h"

#include "logger.h"

#include <QDeclarativeEngine>

#include <assert.h>

CFileSystemModel::CFileSystemModel(QObject *parent):
    QAbstractListModel(parent),
    m_multiple(false),
    m_selectFiles(true),
    m_selectDirectories(false)
#if 0
    m_activeFilterIndex(-1)
#endif
{
    LOG_DBG_FUNC();
    setRoleNames(roleNames());
    m_dir.setFilter(QDir::Dirs | QDir::AllDirs | QDir::Files | QDir::NoDotAndDotDot);
    m_dir.setSorting(QDir::Name | QDir::DirsFirst);
    refreshFilesList();
}

CFileSystemModel::~CFileSystemModel()
{
    LOG_DBG_FUNC();
#if 0
    clearAndReleaseFilters();
#endif
}

QHash<int, QByteArray> CFileSystemModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[FileNameRole] = "name";
    roles[FilePathRole] = "path";
    roles[IsDirectoryRole] = "is_dir";
    roles[CheckableRole] = "checkable";
    roles[CheckedRole] = "checked";

    return roles;
}
#if 0
bool CFileSystemModel::testNameAgainstFilter(QString fileName)
{
    if (m_activeFilterIndex < 0 ||
        m_activeFilterIndex >= m_filters.count())
        return true;

    QJSValue fileNameValue(fileName);

    QJSValueList args;
    args.append(fileNameValue);

    QJSValue ret = m_filters[m_activeFilterIndex].functor.call(args);

    return ret.toBool();
}
#endif
void CFileSystemModel::refreshFilesList()
{
    LOG_DBG_FUNC("in dir %s", m_dir.absolutePath().toUtf8().data());

    m_filesList.clear();

    QFileInfoList files = m_dir.entryInfoList();

    for (QFileInfoList::iterator it = files.begin();
         it != files.end();
         it++)
    {
        LOG_DBG("Checking entry %s which is %s",
                it->fileName().toUtf8().data(),
                it->isDir() ? "dir" : "file");

        if (true /*it->isDir() ||
            testNameAgainstFilter(it->fileName())*/)
        {
            LOG_DBG("Appending %s", it->fileName().toUtf8().data());
            m_filesList.append(*it);
        }
    }

    m_checkStates.clear();
    m_checkStates.fill(false, m_filesList.size());
}

int CFileSystemModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    if (m_dir.isRoot())
        return m_filesList.count();
    else
        return m_filesList.count() + 1;
}

QVariant CFileSystemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int virtualRecordsCount = 0;

    if (!m_dir.isRoot())
        virtualRecordsCount = 1;

    if (index.row() < 0 || index.row() >= m_filesList.count() + virtualRecordsCount)
        return QVariant();

    if (virtualRecordsCount != 0 && index.row() == 0)
    {
        if (role == FileNameRole)
            return "..";
        else if (role == FilePathRole)
            return m_dir.absoluteFilePath("..");
        else if (role == IsDirectoryRole)
            return true;
        else if (role == CheckableRole)
            return false;
        else if (role == CheckedRole)
            return false;
    }
    else
    {
        if (role == FileNameRole)
            return m_filesList[index.row() - virtualRecordsCount].fileName();
        else if (role == FilePathRole)
            return m_filesList[index.row() - virtualRecordsCount].absoluteFilePath();
        else if (role == IsDirectoryRole)
            return m_filesList[index.row() - virtualRecordsCount].isDir();
        else if (role == CheckableRole)
        {
            if (m_filesList[index.row() - virtualRecordsCount].isDir() && m_selectDirectories)
                return true;
            else if (m_filesList[index.row() - virtualRecordsCount].isFile() && m_selectFiles)
                return true;
            else
                return false;
        }
        else if (role == CheckedRole)
            return m_checkStates[index.row() - virtualRecordsCount];
    }

    return QVariant();
}

QString CFileSystemModel::path()
{
    return m_dir.absolutePath();
}

QStringList CFileSystemModel::selectedPaths()
{
    QStringList selectedPaths;

    for (int i = 0; i < m_filesList.count(); i++)
    {
        if (m_checkStates[i])
        {
            selectedPaths.append(m_filesList[i].absoluteFilePath());
        }
    }

    return selectedPaths;
}

void CFileSystemModel::setPath(QString path)
{
    LOG_DBG("Setting path to %s", path.toUtf8().data());

    if (m_dir.cd(path))
    {
        beginResetModel();

        refreshFilesList();

        endResetModel();

        emit pathChanged();
    }
    else
    {
        LOG_ERR("Failed to set path to %s", path.toUtf8().data());
    }
}

bool CFileSystemModel::showHidden()
{
    return m_dir.filter() & QDir::Hidden;
}

void CFileSystemModel::setShowHidden(bool show)
{
    LOG_DBG("%s", show ? "Enabling hidden files" : "Disabling hidden files");

    if (showHidden() == show)
        return;

    beginResetModel();

    if (show)
        m_dir.setFilter(m_dir.filter() | QDir::Hidden);
    else
        m_dir.setFilter(m_dir.filter() & (~QDir::Hidden));

    refreshFilesList();

    endResetModel();

    emit showHiddenChanged();
}

bool CFileSystemModel::multiple()
{
    return m_multiple;
}

void CFileSystemModel::setMultiple(bool multiple)
{
    LOG_DBG("%s", multiple ? "Enabling multiple selection" : "Disabling multiple selection");

    if (multiple == m_multiple)
        return;

    m_multiple = multiple;

    // FIXME: Use more efficient dataChanged()?
    beginResetModel();
    m_checkStates.fill(false);
    endResetModel();

    emit multipleChanged();
}

bool CFileSystemModel::selectFiles()
{
    return m_selectFiles;
}

void CFileSystemModel::setSelectFiles(bool select)
{
    LOG_DBG("%s", select ? "Enabling files selection" : "Disabling files selection");

    if (m_selectFiles == select)
        return;

    m_selectFiles = select;

    beginResetModel();
    m_checkStates.fill(false);
    endResetModel();

    emit selectFilesChanged();
}

bool CFileSystemModel::selectDirectories()
{
    return m_selectDirectories;
}

void CFileSystemModel::setSelectDirectories(bool select)
{
    LOG_DBG("%s", select ? "Enabling dirs selection" : "Disabling dirs selection");

    if (m_selectDirectories == select)
        return;

    m_selectDirectories = select;

    // FIXME: Use more efficient dataChanged()?
    beginResetModel();
    m_checkStates.fill(false);
    endResetModel();

    emit selectDirectoriesChanged();
}
#if 0
int CFileSystemModel::activeFilterIndex()
{
    return m_activeFilterIndex;
}

void CFileSystemModel::setActiveFilterIndex(int index)
{
    LOG_DBG("Setting filter %d as active", index);

    if (index == m_activeFilterIndex)
        return;

    m_activeFilterIndex = index;

    beginResetModel();

    refreshFilesList();

    endResetModel();

    emit activeFilterIndexChanged();
}
#endif
bool CFileSystemModel::setCheckedState(int row, bool checked)
{
    LOG_DBG_FUNC("row=%d, checked=%d", row, checked);

    if (!m_dir.isRoot() && row == 0)
    {
        LOG_WARN("Can't check first row for non-root directory");
        return false;
    }

    int i = row;

    if (!m_dir.isRoot())
    {
        i--;
    }

    if (m_filesList[i].isDir() && !m_selectDirectories)
    {
        LOG_WARN("Can't check row %d because it is dir and directory selection is disabled", row);
        return false;
    }

    if (m_filesList[i].isFile() && !m_selectFiles)
    {
        LOG_WARN("Can't check row %d because it is file and file selection is disabled", row);
        return false;
    }

    if (!m_multiple && checked)
    {
        LOG_DBG("Multiple selection is disabled, unchecking all previously checked entries");
        m_checkStates.fill(false);
    }

    m_checkStates[i] = checked;

    int firstChangedRow = row;
    int lastChangedRow = row;

    if (!m_multiple && checked)
    {
        firstChangedRow = 0;
        lastChangedRow = m_filesList.count() - 1;

        if (!m_dir.isRoot())
            lastChangedRow++;
    }

    QVector<int> changedRoles;
    changedRoles.append(CheckedRole);

    emit dataChanged(index(firstChangedRow), index(lastChangedRow)/*, changedRoles*/);
    emit selectedPathsChanged();

    return true;
}
#if 0
QStringList CFileSystemModel::filterNames()
{
    QStringList filterNames;

    for (int i = 0; i < m_filters.size(); i++)
    {
        filterNames.append(m_filters[i].name);
    }

    return filterNames;
}

int CFileSystemModel::installFilter(QString name, QJSValue functor)
{
    LOG_DBG("Installing filter with name %s", name.toUtf8().data());

    if (!functor.isCallable())
    {
        LOG_WARN("Passed functor is not callable, skipping filter installation");
        return -1;
    }

    acquire(functor.toQObject());

    SFilterRecord record =
    {
        name,
        functor
    };

    m_filters.append(record);

    emit filterNamesChanged();

    beginResetModel();

    refreshFilesList();

    endResetModel();

    return m_filters.count() - 1;
}


void CFileSystemModel::removeFilter(int index)
{
    LOG_DBG("Removing filter with index %d", index);

    if (index < 0 ||
        index >= m_filters.count())
    {
        LOG_WARN("No filter with index %d, can't remove it", index);
        return;
    }

    release(m_filters[index].functor.toQObject());

    m_filters.remove(index);

    emit filterNamesChanged();

    beginResetModel();

    refreshFilesList();

    endResetModel();
}
#endif
void CFileSystemModel::acquire(QObject *object)
{
    LOG_DBG("Aquiring object %p", object);
    QDeclarativeEngine::setObjectOwnership(object, QDeclarativeEngine::CppOwnership);
}

void CFileSystemModel::release(QObject *object)
{
    LOG_DBG("Releasing object %p", object);
    QDeclarativeEngine::setObjectOwnership(object, QDeclarativeEngine::JavaScriptOwnership);
}
#if 0
void CFileSystemModel::clearAndReleaseFilters()
{
    while(!m_filters.empty())
    {
        release(m_filters.last().functor.toQObject());
        m_filters.pop_back();
    }
}
#endif
