#include "logger.h"

#include <stdarg.h>
#include <stdio.h>

static ELoggerSeverity s_outputSeverity;

void LoggerInit(ELoggerSeverity outputSeverity)
{
    s_outputSeverity = outputSeverity;
}

void LoggerOutput(ELoggerSeverity severity, const char *fmt, ...)
{
    if (severity > s_outputSeverity)
        return;

    va_list args;

    va_start(args, fmt);

    vfprintf(stderr, fmt, args);

    va_end(args);
}

void LoggerOutputV(ELoggerSeverity severity, const char *fmt, va_list args)
{
    if (severity > s_outputSeverity)
        return;

    vfprintf(stderr, fmt, args);
}
