import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0

import "../items"

Page {
    id: mainPage

    tools: playerTools

    function runMediaFileDialog(onAccepted, title) {
        var lastOpenedDirectory = DdbApi.confGetStr("filechooser.lastdir", "");

        var filePickerComponent = Qt.createComponent("../dialogs/FilePicker.qml");

        var dialog = filePickerComponent.createObject(mainPage,
                                                      {
                                                          path: lastOpenedDirectory,
                                                          selectDirectories: true,
                                                          multiple: true,
                                                          title: arguments.length >= 2 ? title : ""
                                                      });

        /*
        var supportedFormatsFilter = function(filename) {
            return DdbApi.isFormatSupported(filename);
        };

        var allFilesFilter = function(filename) {
            return true;
        };

        dialog.installFilter(qsTr("Supported formats"), supportedFormatsFilter);
        dialog.installFilter(qsTr("All files"), allFilesFilter);
        */

        dialog.accepted.connect(function(){
            onAccepted(dialog.resultPaths);
        });

        dialog.done.connect(function(){
            DdbApi.confSetStr("filechooser.lastdir", dialog.path);
        });

        dialog.open();
    }

    PageHeader {
        id: header
        title: DdbApi.playlistsModel.currentPlaylistTitle
    }

    ListView {
        anchors {
            left: parent.left
            right: parent.right
            top: header.bottom
            bottom: controlsPanel.top
        }

        clip: true

        Component {
            id: playlistItemDeleteQueryComponent

            QueryDialog {
                acceptButtonText: "Accept"
                rejectButtonText: "Cancel"
            }
        }

        currentIndex: DdbApi.playitemsModel.nowPlayingIdx

        model: DdbApi.playitemsModel

        delegate: ListItem {
            id: listItem

            height: itemColumn.height

            Column {
                id: itemColumn

                width: parent.width

                Item {
                    width: parent.width
                    height: timeLabel.height

                    Label {
                        anchors {
                            left: parent.left
                            leftMargin: AppTheme.horizontalPageMargin
                            right: timeLabel.left
                            rightMargin: AppTheme.paddingMedium
                        }
                        text: firstLine
                        font.pixelSize: 24
                        elide: Text.ElideRight
                        color: listItem.highlighted ? AppTheme.highlightColor :
                               isNowPlaying         ? "blue" :
                                                      AppTheme.primaryColor
                    }

                    Label {
                        id: timeLabel
                        anchors {
                            right: parent.right
                            rightMargin: AppTheme.horizontalPageMargin
                        }
                        text: duration
                        font.pixelSize: 24
                        color: listItem.highlighted ? AppTheme.highlightColor :
                               isNowPlaying         ? "blue" :
                                                      AppTheme.primaryColor
                    }
                }

                Label {
                    anchors {
                        left: parent.left
                        leftMargin: AppTheme.horizontalPageMargin
                        right: parent.right
                        rightMargin: AppTheme.horizontalPageMargin
                    }
                    text: secondLine
                    font.pixelSize: 20
                    elide: Text.ElideRight
                    color: listItem.highlighted ? AppTheme.secondaryHighlightColor :
                           isNowPlaying         ? "darkblue" :
                                                  AppTheme.secondaryColor
                }
            }

            onClicked: DdbApi.playItemIdx(index)
            onPressAndHold: playlistItemContextMenu.open()

            ContextMenu {
                id: playlistItemContextMenu

                visualParent: mainPage

                MenuLayout {
                    MenuItem {
                        text: qsTr("Add To Playback Queue")
                        onClicked: DdbApi.addToPlayqueue(index)
                    }

                    MenuItem {
                        text: qsTr("Remove From Playback Queue")
                        onClicked: DdbApi.removeFromPlayqueue(index)

                        enabled: isInPlayqueue
                        visible: enabled
                    }

                    MenuItem {
                        text: qsTr("Track Properties")
                        onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/TrackProperties.qml"),
                                                  { trackIdx: index })
                    }

                    MenuItem {
                        text: qsTr("Delete")

                        onClicked: {
                            var dialog = playlistItemDeleteQueryComponent.createObject(mainPage);

                            dialog.message = "Do you want to delete track\n%1?".arg(firstLine);

                            dialog.accepted.connect(function () {
                                DdbApi.deleteItemIdx(index);
                            });

                            dialog.open();
                        }
                    }
                }
            }
        }
    }

    Controls {
        id: controlsPanel

        anchors.bottom: parent.bottom

        state: DdbApi.playbackState === CDdbApi.PlaybackStopped ? "stopped" :
               DdbApi.playbackState === CDdbApi.PlaybackPaused  ? "paused" :
                                                                  "playing"

        playbackPosition: DdbApi.playbackPositionMs
        playbackDuration: DdbApi.playbackDurationMs

        onPrevClicked: DdbApi.prev()
        onPlayPauseClicked: DdbApi.playPause()
        onNextClicked: DdbApi.next()

        onSeekPerformed: DdbApi.setPlaybackPosition(position)
    }

    ToolBarLayout {
        id: playerTools

        BackToolIcon {
        }

        ToolIcon {
            iconId: "toolbar-directory"
            onClicked: runMediaFileDialog(function(selectedPaths){
                DdbApi.openPaths(selectedPaths);
            },
            qsTr("Open file(s)/folder(s)", "title of dialog page"))
        }

        ToolIcon {
            iconId: "toolbar-edit"
            onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PlaylistActions.qml"))
        }

        ToolIcon {
            iconId: "toolbar-list"
            onClicked: pageStack.push(Qt.resolvedUrl("../pages/PlaylistsPage.qml"))
        }

        ToolIcon {
            iconId: "toolbar-view-menu"
            onClicked: mainMenu.open()
        }
    }

    Menu {
        id: mainMenu

        MenuLayout {
            MenuItem {
                text: "Settings"
                onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/Settings.qml"))
            }

            MenuItem {
                text: "About"
                onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/About.qml"))
            }
        }
    }
}
