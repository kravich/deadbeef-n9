import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0
import "../items"

Page {
    id: playlistsPage

    tools: ToolBarLayout {
        BackToolIcon {
        }

        ToolIcon {
            iconId: "toolbar-add"
            onClicked: {
                var playlistNameEditComponent = Qt.createComponent("../dialogs/PlaylistNameEdit.qml");

                var dialog = playlistNameEditComponent.createObject(playlistsPage);

                dialog.playlistName = generateNewPlaylistName();
                dialog.creationMode = true;

                dialog.accepted.connect(function () {
                    var playlistIdx = DdbApi.createPlaylist(dialog.playlistName);
                    if (playlistIdx >= 0)
                        DdbApi.selectPlaylist(playlistIdx);
                    pageStack.pop();
                });

                dialog.open();
            }

            function generateNewPlaylistName() {
                var model = playlistsView.model;

                var nameTemplate = qsTr("New playlist");
                var maxInt = Math.pow(2, 53) - 1;

                var proposedName = nameTemplate;

                for (var attempt = 0;; attempt++) {
                    if (attempt > 0)
                        proposedName = nameTemplate + " " + attempt;

                    var found = false;

                    for (var i = 0; i < model.count; i++)
                        if (model.get(i).title === proposedName) {
                            found = true;
                            break;
                        }

                    if (!found || attempt === maxInt)
                        break;
                }

                return proposedName;
            }
        }
    }

    PageHeader {
        id: pageHeader
        title: qsTr("Playlists")
    }

    ListView {
        id: playlistsView

        anchors {
            left: parent.left
            top: pageHeader.bottom
            right: parent.right
            bottom: parent.bottom
        }

        clip: true

        model: DdbApi.playlistsModel

        delegate: ListItem {
            id: listItem

            Label {
                x: AppTheme.horizontalPageMargin
                anchors.verticalCenter: parent.verticalCenter
                text: title
                color: isActive ? "blue" :
                       listItem.highlighted ? AppTheme.highlightColor :
                                              AppTheme.primaryColor
            }

            onClicked: {
                DdbApi.selectPlaylist(index);
                pageStack.pop();
            }

            onPressAndHold: playlistContextMenu.open()

            ContextMenu {
                id: playlistContextMenu

                visualParent: playlistsPage

                MenuLayout {
                    MenuItem {
                        text: qsTr("Rename")
                        onClicked: {
                            // FIXME: Why do we need to save these references?
                            var playlistIdx = index;

                            var playlistNameEditComponent = Qt.createComponent("../dialogs/PlaylistNameEdit.qml");

                            var dialog = playlistNameEditComponent.createObject(playlistsPage);

                            dialog.playlistName = title;

                            dialog.accepted.connect(function() {
                                DdbApi.playlistsModel.setProperty(playlistIdx, "title", dialog.playlistName);
                            });

                            dialog.open();
                        }
                    }

                    MenuItem {
                        text: qsTr("Delete")

                        Component {
                            id: playlistRemoveDialogComponent

                            QueryDialog {
                                // FIXME: Why we can't assign message property here?
                                acceptButtonText: "Delete"
                                rejectButtonText: "Cancel"
                            }
                        }

                        onClicked: {
                            var dialog = playlistRemoveDialogComponent.createObject(playlistsPage);

                            dialog.message = "Do you want to delete playlist \"%1\"?".arg(title);

                            dialog.accepted.connect(function () {
                                DdbApi.deletePlaylist(index);
                            });

                            dialog.open();
                        }
                    }
                }
            }
        }

        ScrollDecorator {}
    }
}
