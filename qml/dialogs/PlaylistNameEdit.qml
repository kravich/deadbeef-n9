import QtQuick 1.0
import com.nokia.meego 1.0

import "../items"

DialogSheet {
    id: playlistCreationDialog

    canAccept: playlistNameLabel.textValid

    property alias playlistName: playlistNameLabel.text
    property bool creationMode: false

    content: Item {
        anchors.fill: parent

        DialogTitle {
            id: dialogTitle
            text: creationMode ? qsTr("Add playlist") : qsTr("Rename playlist")
        }

        Column {
            anchors {
                top: dialogTitle.bottom
                topMargin: AppTheme.paddingLarge
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            spacing: AppTheme.paddingSmall

            LabeledTextField {
                id: playlistNameLabel

                width: parent.width

                focus: true
                label: qsTr("Playlist name")
                placeholderText: label

                property bool textValid: text.length !== 0
    /*
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: playlistCreationDialog.accept()
                EnterKey.enabled: textValid
    */
            }
        }
    }
}
