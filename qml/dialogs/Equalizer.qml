import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

import "../items"

Page {
    // Because Equalizer is quite unhandy in portrait orientation
    allowedOrientations: Orientation.Landscape

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Zero all")
                onClicked: {
                    DdbApi.equalizerModel.preamp = 0.0;
                    DdbApi.equalizerModel.band55Hz = 0.0;
                    DdbApi.equalizerModel.band77Hz = 0.0;
                    DdbApi.equalizerModel.band110Hz = 0.0;
                    DdbApi.equalizerModel.band156Hz = 0.0;
                    DdbApi.equalizerModel.band220Hz = 0.0;
                    DdbApi.equalizerModel.band311Hz = 0.0;
                    DdbApi.equalizerModel.band440Hz = 0.0;
                    DdbApi.equalizerModel.band622Hz = 0.0;
                    DdbApi.equalizerModel.band880Hz = 0.0;
                    DdbApi.equalizerModel.band1200Hz = 0.0;
                    DdbApi.equalizerModel.band1800Hz = 0.0;
                    DdbApi.equalizerModel.band2500Hz = 0.0;
                    DdbApi.equalizerModel.band3500Hz = 0.0;
                    DdbApi.equalizerModel.band5000Hz = 0.0;
                    DdbApi.equalizerModel.band7000Hz = 0.0;
                    DdbApi.equalizerModel.band10000Hz = 0.0;
                    DdbApi.equalizerModel.band14000Hz = 0.0;
                    DdbApi.equalizerModel.band20000Hz = 0.0;
                }
            }
        }

        PageHeader {
            id: header
            title: qsTr("Equalizer")
        }

        TextSwitch {
            id: enableSwitch

            anchors {
                left: parent.left
                right: parent.right
                top: header.bottom
            }

            text: qsTr("Enable")

            automaticCheck: false

            checked: DdbApi.equalizerModel.enabled

            onClicked: DdbApi.equalizerModel.enabled = !DdbApi.equalizerModel.enabled
        }

        SilicaFlickable {
            anchors {
                left: parent.left
                right: parent.right
                top: enableSwitch.bottom
                bottom: parent.bottom
            }

            contentWidth: slidersRow.width + Theme.horizontalPageMargin

            clip: true

            Row {
                id: slidersRow

                height: parent.height

                Item {
                    width: Theme.paddingLarge
                    height: parent.height
                }

                Item {
                    width: Theme.itemSizeMedium

                    height: parent.height

                    VerticalSlider {
                        id: preampSlider

                        anchors.top: parent.top
                        anchors.bottom: preampLabel.top

                        topMargin: Theme.paddingLarge * 2.5
                        bottomMargin: Theme.paddingLarge

                        minimumValue: -20
                        maximumValue: 20

                        stepSize: 1

                        label: qsTr("Preamp")

                        value: DdbApi.equalizerModel.preamp

                        onDownChanged: {
                            if (down) {
                                // Break binding
                                value = value;
                            }
                            else
                            {
                                DdbApi.equalizerModel.preamp = sliderValue;

                                // Restore binding
                                value = Qt.binding(function () {
                                    return DdbApi.equalizerModel.preamp;
                                });
                            }
                        }
                    }

                    Label {
                        id: preampLabel

                        width: parent.width
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: Theme.paddingLarge

                        text: (preampSlider.sliderValue >= 0.0 ? qsTr("+%1 dB") : qsTr("%1 dB")).arg(preampSlider.sliderValue)
                        font.pixelSize: Theme.fontSizeSmall
                        horizontalAlignment: Text.AlignHCenter
                    }
                }

                Item {
                    width: Theme.paddingLarge
                    height: parent.height
                }

                Repeater {
                    model: ListModel {
                        id: ampsModel

                        property var valueBindings: {
                            "55 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band55Hz; }),
                            "77 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band77Hz; }),
                            "110 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band110Hz; }),
                            "156 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band156Hz; }),
                            "220 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band220Hz; }),
                            "311 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band311Hz; }),
                            "440 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band440Hz; }),
                            "622 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band622Hz; }),
                            "880 Hz": Qt.binding(function () { return DdbApi.equalizerModel.band880Hz; }),
                            "1.2 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band1200Hz; }),
                            "1.8 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band1800Hz; }),
                            "2.5 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band2500Hz; }),
                            "3.5 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band3500Hz; }),
                            "5 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band5000Hz; }),
                            "7 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band7000Hz; }),
                            "10 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band10000Hz; }),
                            "14 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band14000Hz; }),
                            "20 kHz": Qt.binding(function () { return DdbApi.equalizerModel.band20000Hz; })
                        }

                        property var setFunctions: {
                            "55 Hz": function (value) { DdbApi.equalizerModel.band55Hz = value; },
                            "77 Hz": function (value) { DdbApi.equalizerModel.band77Hz = value; },
                            "110 Hz": function (value) { DdbApi.equalizerModel.band110Hz = value; },
                            "156 Hz": function (value) { DdbApi.equalizerModel.band156Hz = value; },
                            "220 Hz": function (value) { DdbApi.equalizerModel.band220Hz = value; },
                            "311 Hz": function (value) { DdbApi.equalizerModel.band311Hz = value; },
                            "440 Hz": function (value) { DdbApi.equalizerModel.band440Hz = value; },
                            "622 Hz": function (value) { DdbApi.equalizerModel.band622Hz = value; },
                            "880 Hz": function (value) { DdbApi.equalizerModel.band880Hz = value; },
                            "1.2 kHz": function (value) { DdbApi.equalizerModel.band1200Hz = value; },
                            "1.8 kHz": function (value) { DdbApi.equalizerModel.band1800Hz = value; },
                            "2.5 kHz": function (value) { DdbApi.equalizerModel.band2500Hz = value; },
                            "3.5 kHz": function (value) { DdbApi.equalizerModel.band3500Hz = value; },
                            "5 kHz": function (value) { DdbApi.equalizerModel.band5000Hz = value; },
                            "7 kHz": function (value) { DdbApi.equalizerModel.band7000Hz = value; },
                            "10 kHz": function (value) { DdbApi.equalizerModel.band10000Hz = value; },
                            "14 kHz": function (value) { DdbApi.equalizerModel.band14000Hz = value; },
                            "20 kHz": function (value) { DdbApi.equalizerModel.band20000Hz = value; },
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("55 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("77 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("110 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("156 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("220 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("311 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("440 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("622 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("880 Hz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("1.2 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("1.8 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("2.5 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("3.5 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("5 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("7 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("10 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("14 kHz")
                        }

                        ListElement {
                            sliderLabel: QT_TR_NOOP("20 kHz")
                        }
                    }

                    Item {
                        width: Theme.itemSizeMedium

                        anchors.top: parent.top
                        anchors.bottom: parent.bottom

                        VerticalSlider {
                            id: ampSlider

                            anchors.top: parent.top
                            anchors.bottom: ampLabel.top

                            topMargin: Theme.paddingLarge * 2.5
                            bottomMargin: Theme.paddingLarge

                            minimumValue: -20
                            maximumValue: 20

                            stepSize: 1

                            label: qsTr(sliderLabel)

                            value: ampsModel.valueBindings[sliderLabel]()

                            onDownChanged: {
                                if (down) {
                                    // Break binding
                                    value = value;
                                }
                                else {
                                    ampsModel.setFunctions[sliderLabel](sliderValue);

                                    // Restore binding
                                    value = ampsModel.valueBindings[sliderLabel];
                                }
                            }
                        }

                        Label {
                            id: ampLabel

                            width: parent.width
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: Theme.paddingLarge

                            text: (ampSlider.sliderValue >= 0.0 ? qsTr("+%1 dB") : qsTr("%1 dB")).arg(ampSlider.sliderValue)
                            font.pixelSize: Theme.fontSizeSmall
                            horizontalAlignment: Text.AlignHCenter

                            color: ampSlider.highlighted ? Theme.highlightColor : Theme.primaryColor
                        }
                    }
                }
            }

            HorizontalScrollDecorator {
            }
        }
    }
}
