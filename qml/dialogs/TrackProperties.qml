import QtQuick 1.0
import com.nokia.meego 1.0

import "../items"

import deadbeef 1.0

Page {
    property int trackIdx: -1

    Component.onCompleted: {
        tagsRepeater.model = DdbApi.createTrackTagsModel();
        tagsRepeater.model.idx = trackIdx;

        propertiesRepeater.model = DdbApi.createTrackPropertiesModel();
        propertiesRepeater.model.idx = trackIdx;
    }

    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    PageHeader {
        id: pageHeader
        title: qsTr("Track Properties")
    }

    Flickable {
        anchors {
            left: parent.left
            top: pageHeader.bottom
            right: parent.right
            bottom: parent.bottom
        }

        contentWidth: parent.width
        contentHeight: metaColumn.height

        clip: true

        Column {
            id: metaColumn
            width: parent.width

            SectionHeader { text: qsTr("Metadata") }

            TrackPropertiesPlaceholder {
                enabled: tagsRepeater.count == 0
                text: qsTr("No metadata")
            }

            Repeater {
                id: tagsRepeater

                LabeledTextField {
                    width: parent.width

                    label: qsTr(keyTitle)
                    placeholderText: label
                    text: value

                    readOnly: true
                    //focusOnClick: true
                }
            }

            SectionHeader { text: qsTr("Properties") }

            TrackPropertiesPlaceholder {
                enabled: propertiesRepeater.count == 0
                text: qsTr("No properties")
            }

            Repeater {
                id: propertiesRepeater

                LabeledTextField {
                    width: parent.width

                    label: qsTr(keyTitle)
                    placeholderText: label
                    text: value

                    readOnly: true
                    //focusOnClick: true
                }
            }
        }

        ScrollDecorator {}
    }
}
