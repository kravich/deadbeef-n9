import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0

import "../items"

DialogSheet {
    property string ptr

    Component.onCompleted: {
        optionsRepeater.model = DdbApi.createConfigDialogModel();
        optionsRepeater.model.ptr = ptr;

        dialogTitle.text = qsTr("%1 settings").arg(optionsRepeater.model.pluginName);
    }

    onAccepted: {
        optionsRepeater.model.commitChanges();
    }

    onRejected: {
        optionsRepeater.model.discardChanges();
    }

    content: Item {
        anchors.fill: parent

        DialogTitle {
            id: dialogTitle
        }

        Flickable {
            anchors {
                top: dialogTitle.bottom
                topMargin: AppTheme.paddingLarge
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            contentHeight: controlsColumn.height + AppTheme.paddingLarge

            clip: true

            Column {
                id: controlsColumn

                width: parent.width

                spacing: AppTheme.paddingSmall

                Component {
                    id: entryComponent

                    LabeledTextField {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        placeholderText: label
    /*
                        EnterKey.iconSource: "image://theme/icon-m-enter-close"
                        EnterKey.onClicked: focus = false
    */
                    }
                }

                Component {
                    id: passwordComponent

                    LabeledTextField {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        echoMode: TextInput.Password

                        placeholderText: label
                    }
                }

                Component {
                    id: fileComponent

                    FileSelectionField {
                    }
                }

                Component {
                    id: switchComponent

                    TextSwitch {
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                }

                Component {
                    id: hscaleComponent

                    LabeledSlider {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        valueIndicatorVisible: true
                    }
                }

                Component {
                    id: vscaleComponent

                    // FIXME: Respect property semantics and make it vertical
                    LabeledSlider {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        valueIndicatorVisible: true
                    }
                }

                Component {
                    id: selectComponent

                    ButtonColumn {
                        property variant items: []

                        anchors.horizontalCenter: parent.horizontalCenter

                        Repeater {
                            model: items

                            delegate: Button {
                                text: modelData
                            }
                        }
                    }
                }

                Component {
                    id: unsupportedComponent

                    Label {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.leftMargin: AppTheme.horizontalPageMargin
                        anchors.rightMargin: AppTheme.horizontalPageMargin

                        property string label

                        text: "<Unsupported: " + label + ">"

                        wrapMode: Text.Wrap
                    }
                }

                Repeater {
                    id: optionsRepeater

                    delegate: Loader {
                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        sourceComponent: type === ConfigDialogModel.EntryType    ? entryComponent :
                                         type === ConfigDialogModel.PasswordType ? passwordComponent :
                                         type === ConfigDialogModel.FileType     ? fileComponent :
                                         type === ConfigDialogModel.CheckboxType ? switchComponent :
                                         type === ConfigDialogModel.HscaleType   ? hscaleComponent :
                                         type === ConfigDialogModel.VscaleType   ? vscaleComponent :
                                         type === ConfigDialogModel.SelectType   ? /*selectComponent*/unsupportedComponent :
                                                                                   unsupportedComponent;

                        onLoaded: {
                            if (type === ConfigDialogModel.CheckboxType)
                            {
                                item.text = qsTr(label);
                                item.checked = value;

                                item.checkedChanged.connect(function () {
                                    optionsRepeater.model.setValue(index, item.checked);
                                });
                            }
                            else if (type === ConfigDialogModel.EntryType ||
                                     type === ConfigDialogModel.PasswordType)
                            {
                                item.label = qsTr(label);
                                item.text = value;

                                item.textChanged.connect(function () {
                                    optionsRepeater.model.setValue(index, item.text);
                                });
                            }
                            else if (type === ConfigDialogModel.HscaleType ||
                                     type === ConfigDialogModel.VscaleType)
                            {
                                item.label = qsTr(label);
                                item.minimumValue = scaleMin;
                                item.maximumValue = scaleMax;
                                item.stepSize = scaleStep;
                                item.value = value;

                                item.valueChanged.connect(function () {
                                    optionsRepeater.model.setValue(index, item.value);
                                });
                            }
                            else if (type === ConfigDialogModel.FileType)
                            {
                                item.label = qsTr(label);
                                item.path = value;

                                item.pathChanged.connect(function () {
                                    optionsRepeater.model.setValue(index, item.path);
                                });
                            }
                            else if (type === ConfigDialogModel.SelectType)
                            {
                                // FIXME: Bring up select component
                                /*
                                item.label = qsTr(label);
                                item.items = selectItems;
                                item.currentIndex = value;

                                item.currentIndexChanged.connect(function () {
                                    optionsRepeater.model.setValue(index, item.currentIndex);
                                });
                                */
                            }
                            else
                            {
                                item.label = qsTr(label);
                            }
                        }
                    }
                }
            }

            ScrollDecorator {}
        }
    }
}
