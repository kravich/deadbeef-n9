import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0
import "../items"

Page {
    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    function openUrlExternallyNoTel(link) {
        // Forbid to activate tel. number
        if (link.length >= 3 && link.substring(0, 3) === "tel")
            return;

        Qt.openUrlExternally(link);
    }

    PageHeader {
        id: header
        title: "About"
    }

    Flickable {
        id: pageFlickable

        anchors {
            left: parent.left
            top: header.bottom
            right: parent.right
            bottom: parent.bottom
        }

        contentWidth: parent.width
        contentHeight: contentColumn.height + AppTheme.paddingLarge

        clip: true

        Column {
            id: contentColumn
            width: parent.width

            spacing: AppTheme.paddingLarge

            SectionHeader {
                text: "DeadBeef-N9"
            }

            /*Linked*/Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                }

                /*plainText*/text: qsTr("DeadBeef-N9 GUI plugin %1\n\
Copyright © %2 Evgeny Kravchenko <cravchik@yandex.ru>\n\
This program is licensed under the terms of GPLv3 license\n\
").arg(DdbApi.version).arg("2020")
// ^^^This is so ugly!

                wrapMode: Text.Wrap
                color: AppTheme.primaryColor
                //font.pixelSize: AppTheme.fontSizeSmall

                //defaultLinkActions: false
                //onLinkActivated: openUrlExternallyNoTel(link)
            }

            SectionHeader {
                text: "DeadBeef"
            }

            /*Linked*/Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                }

                function loadTextFile(url, assigner) {
                    var request = new XMLHttpRequest();
                    request.open("GET", url);
                    request.onreadystatechange = function () {
                        if(request.readyState === XMLHttpRequest.DONE) {
                            assigner(request.responseText);
                        }
                    };
                    request.send();
                }

                Component.onCompleted: loadTextFile("file:///usr/share/deadbeef/doc/about.txt",
                                                    function (aboutText) {
                                                        text = aboutText;
                                                    })

                ///*plainText*/text: loadTextFile("file:///usr/share/doc/deadbeef/about.txt")

                wrapMode: Text.Wrap
                color: AppTheme.primaryColor
                //font.pixelSize: AppTheme.fontSizeSmall

                //defaultLinkActions: false
                //onLinkActivated: openUrlExternallyNoTel(link)
            }

            SectionHeader {
                text: qsTr("DeadBeef License")
            }

            /*Linked*/Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                }

                /*plainText*/text: "DeadBeef is licensed under the terms of zlib license.\
For more information please refer to \
https://github.com/DeaDBeeF-Player/deadbeef/blob/master/COPYING"

                wrapMode: Text.Wrap
                color: AppTheme.primaryColor
                //font.pixelSize: AppTheme.fontSizeSmall
            }
        }

        ScrollDecorator {
        }
    }
}
