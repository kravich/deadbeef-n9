import QtQuick 1.0
import com.nokia.meego 1.0

import "../items"

Page {
    property string name
    property string copyright

    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    PageHeader {
        id: pageHeader
        title: name
    }

    Flickable {
        anchors {
            left: parent.left
            top: pageHeader.bottom
            right: parent.right
            bottom: parent.bottom
        }

        contentHeight: copyrightInfoColumn.height + AppTheme.paddingLarge

        clip: true

        Column {
            id: copyrightInfoColumn

            width: parent.width

            spacing: AppTheme.paddingLarge

            SectionHeader {
                text: qsTr("Copyright")
            }

            /*Linked*/Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                }

                /*plainText*/text: copyright
                color: AppTheme.primaryColor
                wrapMode: Text.Wrap
            }
        }

        ScrollDecorator {}
    }

}
