import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0
import "../items"

Page {
    id: settingsPage

    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    PageHeader {
        id: pageHeader

        title: qsTr("Settings")
    }

    Flickable {
        anchors {
            left: parent.left
            top: pageHeader.bottom
            right: parent.right
            bottom: parent.bottom
        }

        contentHeight: settingsColumn.height + AppTheme.paddingLarge

        clip: true

        Column {
            id: settingsColumn

            width: parent.width

            SectionHeader {
                text: qsTr("Looping")
            }

            ButtonColumn {
                anchors.horizontalCenter: parent.horizontalCenter

                checkedButton: DdbApi.playbackMode === CDdbApi.LoopNone   ? loopNone :
                               DdbApi.playbackMode === CDdbApi.LoopSingle ? loopSingle :
                                                                            loopAll

                Button {
                    id: loopNone
                    text: qsTr("Don't loop")
                    onClicked: DdbApi.playbackMode = CDdbApi.LoopNone
                }

                Button {
                    id: loopSingle
                    text: qsTr("Loop single song")
                    onClicked: DdbApi.playbackMode = CDdbApi.LoopSingle
                }

                Button {
                    id: loopAll
                    text: qsTr("Loop all")
                    onClicked: DdbApi.playbackMode = CDdbApi.LoopAll
                }
            }

            SectionHeader {
                text: qsTr("Playback order")
            }

            ButtonColumn {
                anchors.horizontalCenter: parent.horizontalCenter

                checkedButton: DdbApi.playbackOrder === CDdbApi.OrderLinear        ? orderLinear :
                               DdbApi.playbackOrder === CDdbApi.OrderShuffleTracks ? orderShuffleTracks :
                               DdbApi.playbackOrder === CDdbApi.OrderShuffleAlbums ? orderShuffleAlbums :
                                                                                     orderRandom

                Button {
                    id: orderLinear
                    text: qsTr("Linear")
                    onClicked: DdbApi.playbackOrder = CDdbApi.OrderLinear
                }

                Button {
                    id: orderShuffleTracks
                    text: qsTr("Shuffle tracks")
                    onClicked: DdbApi.playbackOrder = CDdbApi.OrderShuffleTracks
                }

                Button {
                    id: orderShuffleAlbums
                    text: qsTr("Shuffle albums")
                    onClicked: DdbApi.playbackOrder = CDdbApi.OrderShuffleAlbums
                }

                Button {
                    id: orderRandom
                    text: qsTr("Random")
                    onClicked: DdbApi.playbackOrder = CDdbApi.OrderRandom
                }
            }

            SectionHeader {
                text: qsTr("Plugins")
            }


            TextSwitch {
                text: /*qsTr(*/"Resume previous session on startup"/*)*/

                property bool isApiVersionSupported: DdbApi.apiVersion.major === 1 &&
                                                     DdbApi.apiVersion.minor >= 10

                enabled: isApiVersionSupported
                visible: enabled

                checked: isApiVersionSupported ? DdbApi.confGetInt("resume_last_session", 1) : false
                onCheckedChanged: if (isApiVersionSupported) {
                                      DdbApi.confSetInt("resume_last_session", checked === true ? 1 : 0)
                                  }
            }

            Item {
                width: parent.width
                height: AppTheme.paddingLarge
            }

            Column {
                width: parent.width

                spacing: AppTheme.paddingLarge

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter

                    function findPluginByName(pluginName) {
                        for (var i = 0; i < DdbApi.pluginsModel.count; i++) {
                            var plugin = DdbApi.pluginsModel.get(i);

                            if (plugin.name === pluginName)
                                return plugin;
                        }

                        return null;
                    }

                    property string lastFmPluginName: "last.fm scrobbler"

                    enabled: {
                        var plugin = findPluginByName(lastFmPluginName);
                        return plugin !== null && plugin.hasSettings;
                    }

                    visible: enabled

                    text: qsTr("Setup Last.fm")
                    onClicked: {
                        var pluginSettingsComponent = Qt.createComponent("../dialogs/PluginSettings.qml");

                        var dialog = pluginSettingsComponent.createObject(settingsPage, {
                                                                              ptr: findPluginByName(lastFmPluginName).ptr
                                                                          });

                        dialog.open();
                    }
                }

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter

                    text: qsTr("Plugins")
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/Plugins.qml"))
                }
            }
        }

        ScrollDecorator {}
    }
}
