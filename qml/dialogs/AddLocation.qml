import QtQuick 1.0
import com.nokia.meego 1.0

import "../items"

DialogSheet {
    id: addLocationDialog

    canAccept: locationLabel.textValid

    property string location: locationLabel.text

    content: Item {
        anchors.fill: parent

        DialogTitle {
            id: dialogTitle
            anchors.top: parent.top
            text: qsTr("Add location")
        }

        Column {
            anchors {
                top: dialogTitle.bottom
                topMargin: AppTheme.paddingLarge
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            spacing: AppTheme.paddingSmall

            LabeledTextField {
                id: locationLabel

                width: parent.width

                focus: true
                label: qsTr("Location:")
                placeholderText: qsTr("URI or file path")
                inputMethodHints: Qt.ImhNoAutoUppercase

                property bool textValid: text.length !== 0
    /*
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: addLocationDialog.accept()
                EnterKey.enabled: textValid
    */
            }
        }
    }
}
