import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0
import "../items"

Page {
    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    PageHeader {
        id: pageHeader
        title: qsTr("Plugins")
    }

    ListView {
        anchors {
            left: parent.left
            top: pageHeader.bottom
            right: parent.right
            bottom: parent.bottom
        }

        clip: true

        model: DdbApi.pluginsModel

        delegate: ListItem {
            id: listItem

            Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                text: name

                color: listItem.highlighted ? AppTheme.highlightColor : AppTheme.primaryColor
            }

            onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PluginInfo.qml"), {
                                          ptr: ptr,
                                          name: name,
                                          description: description,
                                          copyright: copyright,
                                          website: website,
                                          versionMajor: versionMajor,
                                          versionMinor: versionMinor,
                                          hasSettings: hasSettings
                                      })
        }

        ScrollDecorator {}
    }
}
