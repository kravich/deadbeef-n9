import QtQuick 1.0
import com.nokia.meego 1.0

import "../items"

Page {
    id: pluginInfoPage

    property string ptr
    property string name
    property string description
    property string copyright
    property string website
    property int versionMajor
    property int versionMinor
    property bool hasSettings

    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    PageHeader {
        id: pageHeader
        title: name
    }

    Flickable {
        anchors {
            left: parent.left
            top: pageHeader.bottom
            right: parent.right
            bottom: parent.bottom
        }

        contentHeight: pluginInfoColumn.height + AppTheme.paddingLarge

        clip: true

        Column {
            id: pluginInfoColumn
            width: parent.width

            spacing: AppTheme.paddingLarge

            SectionHeader {
                text: qsTr("Description")
            }

            /*Linked*/Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                }

                /*plainText*/text: qsTr(description)
                color: AppTheme.primaryColor
                wrapMode: Text.Wrap
            }

            Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                }

                text: qsTr("Version") + ": " + versionMajor + "." + versionMinor
                color: AppTheme.primaryColor
                wrapMode: Text.Wrap
            }

            SectionHeader {
                text: qsTr("Website")
            }

            /*Linked*/Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                }

                /*plainText*/text: website
                color: AppTheme.primaryColor
                wrapMode: Text.Wrap
            }

            Item {
                width: parent.width
                height: AppTheme.paddingLarge
            }

            Column {
                width: parent.width

                spacing: AppTheme.paddingLarge

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Settings")
                    enabled: hasSettings
                    onClicked: {
                        var pluginSettingsComponent = Qt.createComponent("../dialogs/PluginSettings.qml");

                        var dialog = pluginSettingsComponent.createObject(pluginInfoPage, {
                                                                              ptr: ptr
                                                                          });

                        dialog.open();
                    }
                }

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Copyright")
                    onClicked: pageStack.push(Qt.resolvedUrl("../dialogs/PluginCopyright.qml"), {
                                                  name: name,
                                                  copyright: copyright
                                              })
                }
            }
        }

        ScrollDecorator {}
    }
}
