import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0
import "../items"

Page {
    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    PageHeader {
        id: header

        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        title: qsTr("Sort by")
    }

    TextSwitch {
        id: orderSwitch

        anchors {
            left: parent.left
            right: parent.right
            top: header.bottom
            topMargin: AppTheme.paddingLarge
        }

        text: qsTr("Reverse order")
    }

    ListView {
        anchors {
            left: parent.left
            right: parent.right
            top: orderSwitch.bottom
            topMargin: AppTheme.paddingLarge
            bottom: parent.bottom
        }

        clip: true

        model: ListModel {
            id: sortOptionsModel

            property variant labels: {
                "%artist% %album% %tracknumber%": qsTr("Artist/Album/Track number"),
                "%artist% %album%": qsTr("Artist/Album"),
                "%title%": qsTr("Title"),
                "%tracknumber%": qsTr("Track number"),
                "%album%": qsTr("Album"),
                "%artist%": qsTr("Artist"),
                "%album artist%": qsTr("Album artist"),
                "%date%": qsTr("Date"),
                "%filename%": qsTr("Filename")
            }

            ListElement {
                format: "%artist% %album% %tracknumber%"
            }

            ListElement {
                format: "%artist% %album%"
            }

            ListElement {
                format: "%title%"
            }

            ListElement {
                format: "%tracknumber%"
            }

            ListElement {
                format: "%album%"
            }

            ListElement {
                format: "%artist%"
            }

            ListElement {
                format: "%album artist%"
            }

            ListElement {
                format: "%date%"
            }

            ListElement {
                format: "%filename%"
            }
        }

        delegate: ListItem {
            id: listItem

            Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                text: sortOptionsModel.labels[format]
                color: listItem.highlighted ? AppTheme.highlightColor :
                                              AppTheme.primaryColor
            }

            onClicked: {
                DdbApi.playitemsModel.sortByTf(format,
                                               orderSwitch.checked ? PlayitemsModel.SortDescending :
                                                                     PlayitemsModel.SortAscending);
                pageStack.pop();
            }
        }

        ScrollDecorator {}
    }
}
