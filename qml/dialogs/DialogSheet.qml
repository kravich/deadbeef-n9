import QtQuick 1.0
import com.nokia.meego 1.0

Sheet {
    id: dialogSheet

    property bool canAccept: true

    signal done

    acceptButtonText: "Accept"
    rejectButtonText:  "Cancel"

    function accept() {
        if (!canAccept)
            return;

        done();
        close();
        accepted();
    }

    function reject() {
        done();
        close();
        rejected();
    }

    platformStyle: SheetStyle{
    }

    buttons: [
        SheetButton {
            anchors {
                left: parent.left
                leftMargin: dialogSheet.platformStyle.rejectButtonLeftMargin
                verticalCenter: parent.verticalCenter
            }

            text: dialogSheet.rejectButtonText

            onClicked: dialogSheet.reject()
        },

        SheetButton {
            anchors {
                right: parent.right
                rightMargin: dialogSheet.platformStyle.acceptButtonRightMargin
                verticalCenter: parent.verticalCenter
            }

            platformStyle: SheetButtonAccentStyle {
                background: "image://theme/color8-meegotouch-sheet-button-accent-background"
                pressedBackground: "image://theme/color8-meegotouch-sheet-button-accent-background-pressed"
                disabledBackground: "image://theme/color8-meegotouch-sheet-button-accent-background-disabled"
            }

            enabled: canAccept

            text: dialogSheet.acceptButtonText

            onClicked: dialogSheet.accept()
        }
    ]
}
