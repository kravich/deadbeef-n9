import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0
import "../items"

Page {
    id: actionsPage

    tools: ToolBarLayout {
        BackToolIcon {
        }
    }

    function runMediaFileDialog(onAccepted, title, onDone) {
        // FIXME: Why do we need to save reference to DdbApi
        //        instance to avoid referencing 'undefined'?
        var ddbApi = DdbApi;

        var lastOpenedDirectory = ddbApi.confGetStr("filechooser.lastdir", "");

        var filePickerComponent = Qt.createComponent("../dialogs/FilePicker.qml");

        var dialog = filePickerComponent.createObject(actionsPage,
                                                      {
                                                          path: lastOpenedDirectory,
                                                          selectDirectories: true,
                                                          multiple: true,
                                                          title: arguments.length >= 2 ? title : ""
                                                      });

        /*
        var supportedFormatsFilter = function(filename) {
            return ddbApi.isFormatSupported(filename);
        };

        var allFilesFilter = function(filename) {
            return true;
        };

        dialog.installFilter(qsTr("Supported formats"), supportedFormatsFilter);
        dialog.installFilter(qsTr("All files"), allFilesFilter);
        */

        dialog.accepted.connect(function() {
            onAccepted(dialog.resultPaths);
        });

        var hasOnDone = arguments.length >= 3;

        dialog.done.connect(function() {
            ddbApi.confSetStr("filechooser.lastdir", dialog.path);
            if (hasOnDone) {
                onDone();
            }
        });

        dialog.open();
    }

    function runLoadPlaylistFileDialog(onAccepted, title, onDone) {
        var lastOpenedDirectory = DdbApi.confGetStr("filechooser.playlist.lastdir", "");

        var filePickerComponent = Qt.createComponent("../dialogs/FilePicker.qml");

        var dialog = filePickerComponent.createObject(actionsPage,
                                                      {
                                                          path: lastOpenedDirectory,
                                                          title: arguments.length >= 2 ? title : ""
                                                      });

        /*
        function getExtension(filename) {
            var elements = filename.split(".");

            if (elements.length === 1 || (elements[0] === "" && elements.length === 2))
            {
                return "";
            }

            return elements.pop();
        }

        var supportedPlaylistFormatsFilter = function(filename) {
            var ext = getExtension(filename);
            return DdbApi.getSupportedPlaylistExtensions().indexOf(ext) !== -1;
        };

        var allFilesFilter = function() {
            return true;
        };

        dialog.installFilter(qsTr("Supported playlist formats"), supportedPlaylistFormatsFilter);
        dialog.installFilter(qsTr("All files"), allFilesFilter);
        */

        dialog.accepted.connect(function() {
            onAccepted(dialog.resultPaths[0]);
        });

        var hasOnDone = arguments.length >= 3;

        dialog.done.connect(function() {
            DdbApi.confSetStr("filechooser.playlist.lastdir", dialog.path);
            if (hasOnDone) {
                onDone();
            }
        });

        dialog.open();
    }

    function runSavePlaylistFileDialog(onAccepted, title) {
        var ddbApi = DdbApi;

        var lastOpenedDirectory = ddbApi.confGetStr("filechooser.playlist.lastdir", "");

        var fileSaveComponent = Qt.createComponent("../dialogs/FileSaveDialog.qml");

        var dialog = fileSaveComponent.createObject(actionsPage,
                                                    {
                                                        path: lastOpenedDirectory,
                                                        title: arguments.length >= 2 ? title : ""
                                                    });

        ddbApi.getSupportedPlaylistExtensions().forEach(function(ext) {
            dialog.installExt(ext, ext);
        });

        dialog.accepted.connect(function() {
            onAccepted(dialog.resultPath);
        });

        dialog.done.connect(function() {
            ddbApi.confSetStr("filechooser.playlist.lastdir", dialog.path);
        });

        dialog.open();
    }

    PageHeader {
        id: pageHeader
        title: qsTr("Playlist actions")
    }

    ListView {
        anchors {
            left: parent.left
            top: pageHeader.bottom
            right: parent.right
            bottom: parent.bottom
        }

        clip: true

        model: ListModel {
            id: actionsModel

            property variant labels: {
                "add-files": qsTr("Add file(s)/folder(s)", "playlist actions menu item"),
                "add-location": qsTr("Add location"),
                "load": qsTr("Load playlist", "playlist actions menu item"),
                "save": qsTr("Save playlist", "playlist actions menu item"),
                "sort": qsTr("Sort playlist"),
                "clear": qsTr("Clear playlist")
            }
/*
            property variant actions: {
                "add-files": function() {
                    var ddbApi = DdbApi;
                    runMediaFileDialog(function(selectedPaths) {
                        ddbApi.addPaths(selectedPaths);
                    },
                    qsTr("Add file(s)/folder(s)", "file add dialog title"));
                },

                "add-location": function() {
                    var ddbApi = DdbApi;

                    var dialog = pageStack.replace(Qt.resolvedUrl("../dialogs/AddLocation.qml"));

                    dialog.accepted.connect(function() {
                        ddbApi.addLocation(dialog.location);
                    });
                },

                "save": function() {
                    var ddbApi = DdbApi;
                    runSavePlaylistFileDialog(function(newFilePath) {
                        ddbApi.savePlaylist(newFilePath);
                    },
                    qsTr("Save playlist", "save playlist dialog title"));
                },

                "sort": function() {
                    pageStack.replace(Qt.resolvedUrl("../dialogs/PlaylistSorting.qml"));
                },

                "clear": function() {
                    DdbApi.clearPlaylist();
                    pageStack.pop();
                }
            }
*/
            ListElement {
                name: "add-files"
            }

            ListElement {
                name: "add-location"
            }

            ListElement {
                name: "load"
            }

            /*
              FIXME: Finish porting FileSaveDialog
            ListElement {
                name: "save"
            }
            */

            ListElement {
                name: "sort"
            }

            ListElement {
                name: "clear"
            }
        }

        delegate: ListItem {
            id: listItem

            Label {
                anchors {
                    left: parent.left
                    leftMargin: AppTheme.horizontalPageMargin
                    right: parent.right
                    rightMargin: AppTheme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                text: actionsModel.labels[name]
                color: listItem.highlighted ? AppTheme.highlightColor :
                                              AppTheme.primaryColor
            }

            onClicked: {
                // For some reason, 'actions' dictionary above does not work in Qt4
                // Don't want to figure out the reasons. Just hack it in a following way.
                var actions = {
                    "add-files": function() {
                        runMediaFileDialog(function(selectedPaths) {
                            DdbApi.addPaths(selectedPaths);
                        },
                        qsTr("Add file(s)/folder(s)", "file add dialog title"),
                        function () {
                            // FIXME: Use immediate pop?
                            pageStack.pop();
                        });
                    },

                    "add-location": function() {
                        var addLocationComponent = Qt.createComponent("../dialogs/AddLocation.qml");

                        var dialog = addLocationComponent.createObject(actionsPage);

                        dialog.accepted.connect(function() {
                            DdbApi.addLocation(dialog.location);
                        });

                        dialog.done.connect(function() {
                            pageStack.pop();
                        });

                        dialog.open();
                    },

                    "load": function () {
                        var ddbApi = DdbApi;
                        runLoadPlaylistFileDialog(function(selectedPath) {
                            ddbApi.loadPlaylist(selectedPath);
                        },
                        qsTr("Load playlist", "title of dialog page"),
                        function () {
                            // FIXME: Use immediate pop?
                            pageStack.pop();
                        });
                    },

                    "save": function() {
                        var ddbApi = DdbApi;
                        runSavePlaylistFileDialog(function(newFilePath) {
                            ddbApi.savePlaylist(newFilePath);
                        },
                        qsTr("Save playlist", "save playlist dialog title"));
                    },

                    "sort": function() {
                        pageStack.replace(Qt.resolvedUrl("../dialogs/PlaylistSorting.qml"));
                    },

                    "clear": function() {
                        DdbApi.clearPlaylist();
                        pageStack.pop();
                    }
                }

                actions[name]();
            }
        }

        ScrollDecorator {}
    }
}
