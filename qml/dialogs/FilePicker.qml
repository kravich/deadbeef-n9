import QtQuick 1.0
import com.nokia.meego 1.0

// FIXME: Refactor FileSystemModel into separate module
import deadbeef 1.0

import "../items"

DialogSheet {
    property alias selectFiles: fileSystemModel.selectFiles
    property alias selectDirectories: fileSystemModel.selectDirectories

    property alias multiple: fileSystemModel.multiple
    property alias showHidden: fileSystemModel.showHidden

    property alias path: fileSystemModel.path

    property variant resultPaths: []

    //property alias filterNames: fileSystemModel.filterNames

    property alias title: dialogTitle.text

    function installFilter(name, functor) {
        return fileSystemModel.installFilter(name, functor);
    }

    function removeFilter(index) {
        fileSystemModel.removeFilter(index);
    }

    onDone: {
        if (fileSystemModel.selectedPaths.length !== 0)
        {
            resultPaths = fileSystemModel.selectedPaths;
        }
        else
        {
            resultPaths = [path];
        }
    }

    canAccept: fileSystemModel.selectedPaths.length !== 0 || selectDirectories

    acceptButtonText: selectDirectories && fileSystemModel.selectedPaths.length === 0 ? qsTr("Select current dir") :
                                                                                        qsTr("Select")

    FileSystemModel {
        id: fileSystemModel
        /*
        activeFilterIndex: filtersComboBox.currentIndex
        */
    }

    content: Item {
        anchors.fill: parent
/*
        PullDownMenu {
            MenuItem {
                text: fileSystemModel.showHidden ? qsTr("Hide system files") :
                                                   qsTr("Show system files")

                onClicked: fileSystemModel.showHidden = !fileSystemModel.showHidden
            }
        }
*/
        DialogTitle {
            id: dialogTitle
            anchors.top: parent.top
        }
/*
        ComboBox {
            id: filtersComboBox

            visible: fileSystemModel.filterNames.length !== 0

            width: parent.width
            anchors.top: dialogTitle.bottom
            anchors.topMargin: Theme.paddingLarge

            label: qsTr("Filter")

            menu: ContextMenu {
                Repeater {
                    model: fileSystemModel.filterNames

                    delegate: MenuItem {
                        text: modelData
                    }
                }
            }
        }
*/

        Label {
            id: pathLabel

            //anchors.top: filtersComboBox.bottom
            anchors.top: dialogTitle.bottom
            anchors.left: parent.left
            anchors.leftMargin: AppTheme.horizontalPageMargin
            anchors.right: parent.right
            anchors.rightMargin: AppTheme.horizontalPageMargin

            clip: true

            text: qsTr("Path") + ": " + fileSystemModel.path

            color: AppTheme.primaryColor
        }

        ListView {
            width: parent.width
            anchors.top: pathLabel.bottom
            anchors.topMargin: AppTheme.paddingMedium
            anchors.bottom: parent.bottom

            clip: true

            model: fileSystemModel

            delegate: ListItem {
                id: listItem

                Image {
                    id: icon
                    anchors.left: parent.left
                    anchors.leftMargin: AppTheme.paddingSmall
                    anchors.verticalCenter: parent.verticalCenter
                    width: height
                    height: parent.height

                    source: is_dir ? "image://theme/icon-m-toolbar-directory" : "image://theme/icon-m-content-document"
                    fillMode: Image.PreserveAspectFit
                }

                Label {
                    id: nameLabel

                    anchors.left: icon.right
                    anchors.leftMargin: AppTheme.paddingSmall
                    anchors.right: selectionSwitch.left
                    anchors.rightMargin: AppTheme.paddingSmall
                    anchors.verticalCenter: parent.verticalCenter

                    text: name
                    color: listItem.highlighted ? AppTheme.highlightColor : AppTheme.primaryColor

                    elide: Text.ElideMiddle
                }

                Switch {
                    id: selectionSwitch
                    anchors.right: parent.right
                    anchors.rightMargin: AppTheme.horizontalPageMargin
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    width: height

                    visible: checkable

                    automaticCheck: false
                    checked: model.checked

                    onClicked: {
                        if (checked)
                        {
                            fileSystemModel.setCheckedState(index, false);
                        }
                        else
                        {
                            fileSystemModel.setCheckedState(index, true);
                        }
                    }
                }

                onClicked: {
                    if (is_dir)
                    {
                        fileSystemModel.path = path;
                    }
                }
            }

            ScrollDecorator {}
        }
    }
}
