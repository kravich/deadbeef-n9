import QtQuick 2.0
import Sailfish.Silica 1.0

import deadbeef 1.0

CoverBackground {
    Loader {
        anchors {
            left: parent.left
            leftMargin: Theme.paddingLarge
            right: parent.right
            rightMargin: Theme.paddingLarge
            top: parent.top
            topMargin: Theme.paddingMedium
            bottom: coverActionArea.top
            bottomMargin: Theme.paddingMedium
        }

        sourceComponent: DdbApi.playbackState === DdbApi.PlaybackStopped ? stoppedItem :
                                                                           nowPlayingItem
    }

    Component {
        id: stoppedItem

        Item {
            Label {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                text: "DeadBeef"
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    Component {
        id: nowPlayingItem

        Item {
            property real textImplicitHeight: tracknameLabel.implicitHeight + artistAlbumLabel.implicitHeight
            property bool textFits: textImplicitHeight <= height

            Label {
                id: tracknameLabel

                anchors.left: parent.left
                anchors.right: parent.right

                y: textFits ? (parent.height - textImplicitHeight) / 2 : 0

                height: {
                    if (textFits || implicitHeight < parent.height / 2)
                        return implicitHeight;

                    if (artistAlbumLabel.implicitHeight >= parent.height / 2)
                        return parent.height / 2;

                    return parent.height - artistAlbumLabel.implicitHeight;
                }

                text: DdbApi.playitemsModel.nowPlayingtTitle
                color: Theme.primaryColor
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom
            }

            Label {
                id: artistAlbumLabel

                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: tracknameLabel.bottom

                height: {
                    if (textFits || implicitHeight < parent.height / 2)
                        return implicitHeight;

                    if (tracknameLabel.implicitHeight >= parent.height / 2)
                        return parent.height / 2;

                    return parent.height - tracknameLabel.implicitHeight;
                }

                text: DdbApi.playitemsModel.nowPlayingArtistAlbum
                color: Theme.secondaryColor
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignTop
            }
        }
    }

    CoverActionList {
        id: coverActions

        CoverAction {
            iconSource: "image://theme/icon-cover-" + (DdbApi.playbackState == DdbApi.PlaybackPlaying ? "pause" :
                                                                                                        "play")
            onTriggered: DdbApi.playPause()
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-next-song"
            onTriggered: DdbApi.next()
        }
    }
}
