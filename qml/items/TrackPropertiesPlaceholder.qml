import QtQuick 1.0
import com.nokia.meego 1.0

Item {
    id: root

    property string text: "<Placeholder text>"

    anchors {
        left: parent.left
        right: parent.right
    }

    height: enabled ? placeholderLabel.height : 0
    visible: enabled

    Label {
        id: placeholderLabel

        anchors {
            left: parent.left
            leftMargin: AppTheme.horizontalPageMargin
            right: parent.right
            rightMargin: AppTheme.horizontalPageMargin
        }

        text: root.text
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.Wrap
        color: AppTheme.secondaryHighlightColor
        font.pixelSize: AppTheme.fontSizeLarge
        //font.family: AppTheme.fontFamilyHeading
    }
}
