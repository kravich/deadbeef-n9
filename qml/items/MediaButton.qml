import QtQuick 1.1
import com.nokia.meego 1.0

MouseArea {
    property alias source: image.source

    implicitWidth: image.implicitWidth
    implicitHeight: image.implicitHeight

    Image {
        id: image
        anchors.centerIn: parent
    }
}
