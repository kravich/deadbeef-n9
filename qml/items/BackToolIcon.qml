import QtQuick 1.0
import com.nokia.meego 1.0

ToolIcon {
    iconId: pageStack && pageStack.depth > 1 ? "toolbar-back" : "toolbar-back-dimmed"
    onClicked: {
        if (pageStack && pageStack.depth > 1)
            pageStack.pop();
    }
}
