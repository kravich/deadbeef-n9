import QtQuick 1.0
import com.nokia.meego 1.0

Column {
    property alias label: labelItem.text
    property alias minimumValue: sliderItem.minimumValue
    property alias maximumValue: sliderItem.maximumValue
    property alias stepSize: sliderItem.stepSize
    property alias value: sliderItem.value
    property alias valueIndicatorVisible: sliderItem.valueIndicatorVisible

    Slider {
        id: sliderItem

        width: parent.width
    }

    Label {
        id: labelItem

        anchors.horizontalCenter: parent.horizontalCenter
    }
}
