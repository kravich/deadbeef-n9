import QtQuick 1.0
import com.nokia.meego 1.0

import deadbeef 1.0

Label {
    x: AppTheme.horizontalPageMargin
    width: (parent ? parent.width : Screen.width) - 2 * x

    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignLeft

    color: AppTheme.primaryColor
    // FIXME: Use size from AppTheme
    font.pixelSize: 32
}
