import QtQuick 1.0
import com.nokia.meego 1.0

Column {
    id: root

    signal prevClicked
    signal playPauseClicked
    signal nextClicked
    signal seekPerformed(real position)

    property real playbackPosition: 0.0
    property real playbackDuration: 0.0

    property bool seekPossible: playbackDuration !== 0

    width: parent.width

    state: "stopped"

    Item {
        width: parent.width

        height: Math.max(prevButton.height,
                         playPauseButton.height,
                         nextButton.height)

        MediaButton {
            id: prevButton

            anchors.left: parent.left
            width: implicitWidth + 2 * AppTheme.horizontalPageMargin
            height: implicitHeight + 2 * AppTheme.paddingLarge

            source: "image://theme/icon-m-toolbar-mediacontrol-previous" + (pressed ? "-dimmed" : "")
            onClicked: root.prevClicked()
        }

        MediaButton {
            id: playPauseButton

            anchors.horizontalCenter: parent.horizontalCenter
            width: implicitWidth + 2 * AppTheme.horizontalPageMargin
            height: implicitHeight + 2 * AppTheme.paddingLarge

            source: "image://theme/icon-m-toolbar-mediacontrol-play" + (pressed ? "-dimmed" : "")
            onClicked: root.playPauseClicked()
        }

        MediaButton {
            id: nextButton

            anchors.right: parent.right
            width: implicitWidth + 2 * AppTheme.horizontalPageMargin
            height: implicitHeight + 2 * AppTheme.paddingLarge

            source: "image://theme/icon-m-toolbar-mediacontrol-next" + (pressed ? "-dimmed" : "")
            onClicked: root.nextClicked()
        }
    }

    Seekbar {
        id: seekbar

        width: parent.width

        // When seek is not possible, set maximumValue to something larger than value
        // to avoid warning
        maximumValue: seekPossible ? playbackDuration : 1.0

        enabled: seekPossible

        durationText: playbackDuration > 0.0 ? msToMinutesAndSeconds(playbackDuration) : "-:--"

        Binding {
            id: valueBinding
            target: seekbar
            when: !seekbar.pressed
            property: "value"
            value: seekPossible ? playbackPosition : 0.0
        }

        onValueChanged: if (pressed) { root.seekPerformed(value) }

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        function msToMinutesAndSeconds(ms) {
            var minutes = Math.floor(ms / 1000 / 60);
            var seconds = Math.floor(ms / 1000 % 60);

            return minutes + ":" + pad(seconds, 2);
        }
    }

    states: [
        State {
            name: "stopped"
            PropertyChanges {
                target: playPauseButton
                source: "image://theme/icon-m-toolbar-mediacontrol-play" + (pressed ? "-dimmed" : "")
            }

            PropertyChanges {
                target: seekbar
                positionText: "-:--"
            }

        },

        State {
            name: "paused"
            PropertyChanges {
                target: playPauseButton
                source: "image://theme/icon-m-toolbar-mediacontrol-play" + (pressed ? "-dimmed" : "")
            }

            PropertyChanges {
                target: seekbar
                positionText: seekPossible ? msToMinutesAndSeconds(playbackPosition) : "-:--"
            }

        },

        State {
            name: "playing"
            PropertyChanges {
                target: playPauseButton
                source: "image://theme/icon-m-toolbar-mediacontrol-pause" + (pressed ? "-dimmed" : "")
            }

            PropertyChanges {
                target: seekbar
                positionText: seekPossible ? msToMinutesAndSeconds(playbackPosition) : "-:--"
            }

        }

    ]
}
