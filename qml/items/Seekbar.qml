import QtQuick 1.0
import com.nokia.meego 1.0

Item {
    property alias value: progressbar.value

    property alias minimumValue: progressbar.minimumValue
    property alias maximumValue: progressbar.maximumValue

    property alias pressed: seekArea.pressed

    property alias positionText: positionLabel.text
    property alias durationText: durationLabel.text

    width: parent.width
    height: seekArea.height + AppTheme.paddingLarge + Math.max(positionLabel.height, durationLabel.height)

    MouseArea {
        id: seekArea

        anchors {
            left: parent.left
            leftMargin: AppTheme.horizontalPageMargin
            right: parent.right
            rightMargin: AppTheme.horizontalPageMargin
        }

        height: 60 - AppTheme.paddingLarge

        function clamp(val) {
            return Math.max(minimumValue, Math.min(val, maximumValue));
        }

        function updateValue() {
            if (pressed) {
                value = clamp(minimumValue + (maximumValue - minimumValue) * mouseX / width);
            }
        }

        onMouseXChanged: {
            updateValue();
            seekBubble.x = mouseX - seekBubble.width / 2
        }

        ProgressBar {
            id: progressbar

            enabled: parent.enabled

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                bottomMargin: AppTheme.paddingLarge
            }

            SeekBubble {
                id: seekBubble

                anchors.bottom: parent.top
                opacity: seekArea.pressed ? 1 : 0

                value: positionText
            }
        }
    }

    Label {
        id: positionLabel

        anchors {
            left: parent.left
            leftMargin: AppTheme.horizontalPageMargin
            bottom: parent.bottom
            bottomMargin: AppTheme.paddingLarge
        }
    }

    Label {
        id: durationLabel

        anchors {
            right: parent.right
            rightMargin: AppTheme.horizontalPageMargin
            bottom: parent.bottom
            bottomMargin: AppTheme.paddingLarge
        }
    }
}
