import QtQuick 1.0
import com.nokia.meego 1.0

MouseArea {
    property alias text: label.text
    property alias checked: sw.checked

    width: parent.width
    height: Math.max(sw.height, label.height)

    Switch {
        id: sw
        anchors {
            left: parent.left
            leftMargin: AppTheme.horizontalPageMargin
            verticalCenter: parent.verticalCenter
        }
    }

    Label {
        id: label
        anchors {
            left: sw.right
            leftMargin: AppTheme.paddingLarge
            verticalCenter: parent.verticalCenter
        }
    }

    onClicked: checked = !checked
}
