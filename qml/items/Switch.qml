import QtQuick 1.0
import com.nokia.meego 1.0

CheckBox {
    property bool automaticCheck: true

    // Here we override private function which is usually bad
    // but CheckBox implementation won't change anymore anyway
    function __handleChecked() {
        if (automaticCheck) {
            checkbox.checked = !checkbox.checked;
        }
    }
}
