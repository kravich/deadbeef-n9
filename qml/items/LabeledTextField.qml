import QtQuick 1.0
import com.nokia.meego 1.0

Item {
    property alias label: labelItem.text
    property alias text: field.text
    property alias placeholderText: field.placeholderText
    property alias readOnly: field.readOnly
    property alias inputMethodHints: field.inputMethodHints
    property alias echoMode: field.echoMode

    width: AppTheme.horizontalPageMargin + labelItem.width + AppTheme.paddingLarge + field.width + AppTheme.horizontalPageMargin
    height: Math.max(labelItem.height, field.height)

    Label {
        id: labelItem

        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: AppTheme.horizontalPageMargin
        anchors.left: parent.left
    }

    TextField {
        id: field

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: labelItem.right
        anchors.leftMargin: AppTheme.paddingLarge
        anchors.right: parent.right
        anchors.rightMargin: AppTheme.horizontalPageMargin
    }
}
