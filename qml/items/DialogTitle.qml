import QtQuick 1.0
import com.nokia.meego 1.0

Label {
    property int fontSize: AppTheme.fontSizeLarge

    anchors {
        left: parent.left
        leftMargin: AppTheme.horizontalPageMargin
        right: parent.right
        rightMargin: AppTheme.horizontalPageMargin
    }

    font.pixelSize: fontSize
    wrapMode: Text.Wrap
    color: AppTheme.primaryColor
    opacity: text.length > 0 ? 1.0 : 0.0
    /*
    Behavior on opacity {
        FadeAnimation {}
    }
    */
}
