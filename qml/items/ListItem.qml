import com.nokia.meego 1.0
import QtQuick 1.0

MouseArea {
    width: parent.width
    height: 64

    property bool down: pressed && containsMouse

    property bool highlighted: down
    property color highlightedColor: AppTheme.highlightBackgroundColor

    Rectangle {
        anchors.fill: parent
        color: highlighted ? highlightedColor : "transparent"
    }
}
