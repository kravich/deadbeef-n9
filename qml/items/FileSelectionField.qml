import QtQuick 1.0
import com.nokia.meego 1.0

Item {
    property alias label: textField.label
    property alias path: textField.text

    anchors.left: parent.left
    anchors.right: parent.right

    height: textField.height

    LabeledTextField {
        id: textField

        anchors.left: parent.left
        anchors.right: selectionButton.left

        placeholderText: label
/*
        EnterKey.iconSource: "image://theme/icon-m-enter-close"
        EnterKey.onClicked: focus = false
*/
    }

    Button {
        id: selectionButton

        anchors.right: parent.right
        anchors.rightMargin: AppTheme.horizontalPageMargin

        width: height

        text: "..."

        onClicked: {
            var filePickerComponent = Qt.createComponent("../dialogs/FilePicker.qml");

            // FIXME: Support passing current path to file dialog
            var dialog = filePickerComponent.createObject(pageStack.currentPage,
                                                          {
                                                              title: "Select \"" + label + "\""
                                                          });

            dialog.accepted.connect(function () {
                textField.text = dialog.resultPaths[0];
            });

            dialog.open();
        }
    }
}
