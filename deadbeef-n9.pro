TEMPLATE = lib

TARGET = ddb_gui_n9

CONFIG += plugin no_plugin_name_prefix link_pkgconfig meegotouch

QT += declarative
PKGCONFIG += libresourceqt1

# Limit API version
DEFINES += "DDB_API_LEVEL=8"

SOURCES += \
    src/ddb_gui_n9.cpp \
    src/ddbapi.cpp \
    src/filesystemmodel.cpp \
    src/trackmetadatamodel.cpp \
    src/configdialogmodel.cpp \
    src/strutils.cpp \
    src/configschemeparser.cpp \
    src/fs.cpp \
    src/logger.cpp \
    src/abstractqmllistmodel.cpp

# Specify files to parse by lupdate
lupdate_only {
    SOURCES += qml/cover/*.qml \
        qml/dialogs/*.qml \
        qml/items/*.qml \
        qml/pages/*.qml \
        qml/translation/*.qml \
        qml/*.qml
}

RESOURCES += deadbeef-n9.qrc

HEADERS += \
    src/ddbapi.h \
    src/filesystemmodel.h \
    src/trackmetadatamodel.h \
    src/configdialogmodel.h \
    src/strutils.h \
    src/configschemeparser.h \
    src/fs.h \
    src/logger.h \
    src/version.h \
    src/abstractqmllistmodel.h \
    src/apptheme.h

# This is needed by QtCreator to find deadbeef/deadbeef.h file for autocompletion
INCLUDEPATH += /usr/include/

target.path = /usr/lib/deadbeef

desktop.files = deadbeef-n9.desktop
desktop.path = /usr/share/applications

INSTALLS += target desktop

icon80x80.files = icons/80x80/deadbeef-n9.png
icon80x80.path = /usr/share/icons/hicolor/80x80/apps

INSTALLS += icon80x80

LANG_CODES = ru es nb_NO fr zh hu de

for(langCode, LANG_CODES) {
    TRANSLATIONS += translations/deadbeef-n9-$${langCode}.ts
}

OTHER_FILES += \
    qml/translation/ExtraStrings.qml \
    deadbeef-n9.desktop
