# DeadBeef Silica
A Sailfish port of the [DeadBeef](http://deadbeef.sourceforge.net/) audio player.

## Building and deploying
### Preparing build environment

1. Install [Sailfish OS Application SDK](https://sailfishos.org/wiki/Application_SDK_Installation)
2. Run VirtualBox GUI
3. Start "Sailfish OS Build Engine" virtual machine
4. Connect to virtual machine using ssh: `ssh -p 2222 -i ~/SailfishOS/vmshare/ssh/private_keys/engine/mersdk mersdk@localhost`
5. Add custom repository with build dependencies by executing following command in remote console:
`sb2 -t SailfishOS-3.0.1.11-armv7hl -m sdk-install -R zypper ar -G http://repo.merproject.org/obs/home:/kravich:/deadbeef/sailfish_latest_armv7hl/home:kravich:deadbeef.repo`
In your case -t option value may differ. To find out the proper name of armv7hl target you may use `sb2-config -l` command
6. Shut down virtual machine

### Building

1. Run Sailfish OS IDE
2. Open deadbeef-silica.pro project
3. (Only once) Ensure that only **armv7hl** kit is selected and configure project
4. Select "Build"->"Open Build and Run Kit Selector" and in provided window select "Debug" Build type and "Deploy As RPM Package" Deploy configuration
5. Select "Build"->"Build All", confirm virtual machine start and wait until build is finished
6. When build is finished, check build-deadbeef-silica-... directory near the directory where deadbeef-silica.pro file is located. Build artefacts will be located there.

### Installing runtime dependencies
1. The easiest way: just install DeadBeef Silica on your phone [from OpenRepos](https://openrepos.net/content/kravich/deadbeef-silica) and all required dependencies will be installed automatically
2. Manual way: **TBD**

### Deploying single .so for debug purposes
1. Create directory on a Salfish OS device: `mkdir -p ~/.local/lib/deadbeef`
1. Build plugin using "Deploy As RPM Package" Deploy configuration
2. Locate ddb\_gui\_silica.so file in a build directory
3. Copy it to Sailfish OS device: `scp ddb_gui_silica.so nemo@192.168.2.15:.local/lib/deadbeef`, where 192.168.2.15 is ip address of your phone
4. Run DeadBeef from Application Launcher. Updated GUI plugin will be picked up automatically.

### Deploying RPM
**TBD**

## Translations

If you want to translate this application to your language, generate new .ts file from deadbeef-silica.pro using lupdate tool, translate it using Qt5 Linguist and propose for inclusion using Pull Request.

To test your translation, you can turn your .ts file into .qm file using lrelease tool and put it inside ~/.local/share/deadbeef/translations directory. Application will load your translation automatically if current locale matches locale specified in .qm file name.
You can also put your .qm file into /usr/share/deadbeef/translations directory. This may be useful for overriding built-in translations (e.g. in case if some built-in translation is outdated).

Also, a web-based translation tool will be available in some moment.
